package breeze.mobile.rebus

import android.content.Context
import android.graphics.Point
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import breeze.mobile.rebus.manager.DataManager
import breeze.mobile.rebus.models.Definition
import android.widget.*
import breeze.mobile.rebus.models.Square
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.collections.ArrayList
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener


class MainActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    lateinit var navigation_view: NavigationView
    lateinit var keyboardView: KeyboardView
    lateinit var keyboard: Keyboard

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        keyboardView = findViewById(R.id.keyboard_view)
        keyboard = Keyboard(this, R.xml.keys_layout)
        keyboardView.keyboard = keyboard
        keyboardView.isEnabled = true
        keyboardView.isClickable = true
        val keys = keyboard.keys

        val listener = object : OnKeyboardActionListener {

            override fun onKey(primaryCode: Int, keyCodes: IntArray) {

//                Log.e("onKEY", (primaryCode.toChar()).toString())

                if(primaryCode==8888){
                    showKeyboard(false)
                }else
                    if(primaryCode==9999){

                    }else
                        if(primaryCode==8){
                            if(DataManager.currentInput!=null){
                                DataManager.currentInput!!.setText("")
                                val coordinate = DataManager.currentInput!!.coordinate
                                val def = DataManager.currentInput!!.definitionForCoordinateAndDirection(coordinate, DataManager.currentDirection)

                                if(DataManager.currentDirection ==Definition.Direction.Horizontal){

                                    if(coordinate.x == def!!.startCoordinate.x){
                                        //nothing
                                    }else
                                    {
                                        val newInput = DataManager.inputs[coordinate.y][coordinate.x-1]
                                        DataManager.currentInput?.changeSelectedCell(false)
                                        DataManager.currentInput = newInput
                                        DataManager.currentInput?.requestFocus()
                                        DataManager.currentInput?.setSelection(DataManager.currentInput?.text!!.length)
                                        DataManager.currentInput?.changeSelectedCell(true)
                                    }

                                }else {

                                    if (coordinate.y == def!!.startCoordinate.y) {
                                        //nothing
                                    } else {
                                        val newInput = DataManager.inputs[coordinate.y - 1][coordinate.x]
                                        DataManager.currentInput?.changeSelectedCell(false)
                                        DataManager.currentInput = newInput
                                        DataManager.currentInput?.requestFocus()
                                        DataManager.currentInput?.setSelection(DataManager.currentInput?.text!!.length)
                                        DataManager.currentInput?.changeSelectedCell(true)
                                    }
                                }
                            }

                        }else
                            DataManager.currentInput?.setText((primaryCode.toChar()).toString())
            }

            override fun onPress(primaryCode: Int) {
            }

            override fun onRelease(primaryCode: Int) {}

            override fun onText(text: CharSequence) {
                // TODO Auto-generated method stub

            }

            override fun swipeDown() {
                // TODO Auto-generated method stub

            }

            override fun swipeLeft() {
                // TODO Auto-generated method stub

            }

            override fun swipeRight() {
                // TODO Auto-generated method stub

            }

            override fun swipeUp() {
                // TODO Auto-generated method stub

            }

        }

        keyboardView.setOnKeyboardActionListener(listener)

        toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)

        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayUseLogoEnabled(false)
        drawerLayout = findViewById(R.id.drawer_layout)
        actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0)
        actionBarDrawerToggle.isDrawerIndicatorEnabled = true
        actionBarDrawerToggle.syncState()

        drawerLayout.addDrawerListener(actionBarDrawerToggle)

        navigation_view = findViewById(R.id.navigation_view)
        navigation_view.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { item ->

            when (item.itemId) {
                R.id.verify -> {
                    checkpercentage()
                    drawerLayout.closeDrawer(navigation_view)
                }
                else -> return@OnNavigationItemSelectedListener true
            }

            true
        })

        setupGrid()

    }

    private fun showKeyboard(visibility : Boolean){

        if(visibility) {
            keyboardView.visibility = View.VISIBLE
        }else
        {
            keyboardView.visibility = View.GONE
        }
    }

    private fun checkpercentage(){
        var totalFound = 0
        var totalSquares = DataManager.size!!.height * DataManager.size!!.width
        var percentage = 0

        for (j in 0 until DataManager.size!!.height) {
            for (i in 0 until DataManager.size!!.width) {
                if(DataManager.typedCharacters[j][i] == DataManager.rightCharacters[j][i]){
                    if(DataManager.rightCharacters[j][i]==""){
                        totalSquares--
                    }else {
                        totalFound++
                    }
                }
            }
        }

//        Log.e("PERCENTAGE", "$totalFound, $totalSquares")

        percentage = (totalFound * 100) /totalSquares

        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setMessage("The completion percentage is $percentage%")
        val dialog: AlertDialog = builder.create()
        dialog.show()

    }

    private fun setObserver(){
        val observable = DataManager.getObservable()
        val mySubscriber = object: Observer<String> {
            override fun onSubscribe(d: Disposable) {
                Log.e("OBS", "onSubscribe")
            }

            override fun onNext(s: String) {
                Log.e("OBS", s)
                definition_tv.text = s
            }

            override fun onComplete() {
                Log.e("OBS", "onComplete")
            }

            override fun onError(e: Throwable) {
                Log.e("OBS", "onError")
            }

        }


        observable.subscribe(mySubscriber)
    }


    private fun setupGrid() {
        val rebusGrid = findViewById<GridLayout>(R.id.rebusGrid)
        val layoutInflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val displayMetrics = resources.displayMetrics
        val cellWidth: Int = (displayMetrics.widthPixels / DataManager.size!!.width)


        for (j in 0 until DataManager.size!!.height) {
            for (i in 0 until DataManager.size!!.width) {

                val def: Definition? = DataManager.definitionForCoordinateBothDirections(Point(i, j))
                if (def == null) {
                    DataManager.rightCharacters[j][i] = ""
                } else {
                    DataManager.rightCharacters[j][i] = getRightCharacter(def, i, j)
                }

                val defs: ArrayList<Definition>? = DataManager.definitionsForCoordinate(Point(i, j))

                if (defs == null) {
                    val child: View = layoutInflater.inflate(R.layout.empty_cell, null)
                    val params: GridLayout.LayoutParams = GridLayout.LayoutParams(
                        GridLayout.spec(j, GridLayout.CENTER),
                        GridLayout.spec(i, GridLayout.CENTER)
                    )

                    val iv = child.findViewById<ImageView>(R.id.definition_ll)
                    val indexTv = child.findViewById<TextView>(R.id.indexTv)
                    val layoutParams = RelativeLayout.LayoutParams(cellWidth, cellWidth)
                    iv.layoutParams = layoutParams

                    if (i == 0) {
                        indexTv.text = (j + 1).toString()
                    } else
                        if (j == 0) {
                            indexTv.text = (i + 1).toString()
                        }

                    DataManager.inputs[j][i] = null

                    rebusGrid.addView(child, params)

                } else {
                    val child: View = layoutInflater.inflate(R.layout.grid_cell, null)
                    val params: GridLayout.LayoutParams = GridLayout.LayoutParams(
                        GridLayout.spec(j, GridLayout.CENTER),
                        GridLayout.spec(i, GridLayout.CENTER)
                    )

                    val main_rl = child.findViewById<RelativeLayout>(R.id.main_rl)
                    val layoutParams = RelativeLayout.LayoutParams(cellWidth, cellWidth)
                    val indexTv = main_rl.findViewById<TextView>(R.id.indexTv)

                    main_rl.layoutParams = layoutParams

                    rebusGrid.addView(child, params)

                    val editText = child.findViewById<Square>(R.id.cell_et)

                    if (i == 0) {
                        indexTv.text = (j + 1).toString()
                    } else
                        if (j == 0) {
                            indexTv.text = (i + 1).toString()
                        }

                    DataManager.inputs[j][i] = editText

                    editText.coordinate = Point(i, j)

                    editText.definitions = defs

                    editText.showSoftInputOnFocus = false

                    editText.setOnTouchListener { v, event ->
                        when (event?.action) {
                            MotionEvent.ACTION_DOWN -> showKeyboard(true)
                        }

                        v?.onTouchEvent(event) ?: true
                    }

                }

            }
        }

        setObserver()

        readRebus()
    }

    private fun getRightCharacter(def: Definition, i: Int, j: Int): String {

        val answer = def.answer
        val x = def.startCoordinate.x
        val y = def.startCoordinate.y
        val direction = def.direction

        if (direction == Definition.Direction.Horizontal) {
            val index = i - x

            return answer[index].toString()
        } else {
            val index = j - y

            return answer[index].toString()
        }

    }

    fun saveRebus() {
        for (array in DataManager.typedCharacters) {
            for (value in array) {
                print("$value ")
            }
            println()
        }

        val sharedPreferences = getSharedPreferences("myPrefs", MODE_PRIVATE)
        val gson = Gson()
        val json = gson.toJson(DataManager.typedCharacters)
        val editor = sharedPreferences.edit()
        editor.putString(DataManager.id.toString(), json)
        editor.commit()

    }

    fun readRebus(){
        val sharedPreferences = getSharedPreferences("myPrefs", MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString(DataManager.id.toString(), "")
        if (json.isNullOrEmpty())
        {
            Log.e("EMPTY CHARS", DataManager.id.toString())
        } else {
            val type = object : TypeToken<Array<Array<String>>>(){}.type

            val savedCharacters = gson.fromJson<Array<Array<String>>>(json, type)

            DataManager.typedCharacters = savedCharacters

            for (j in 0 until DataManager.size!!.height) {
                for (i in 0 until DataManager.size!!.width) {
                    DataManager.inputs[j][i]?.setText(savedCharacters[j][i])
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()

        saveRebus()
        DataManager.resetData()

    }

    override fun onPause() {
        super.onPause()

        saveRebus()

    }


}

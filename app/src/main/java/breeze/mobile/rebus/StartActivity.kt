package breeze.mobile.rebus

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import breeze.mobile.rebus.manager.DataManager
import com.google.android.material.navigation.NavigationView

class StartActivity : AppCompatActivity() {

    var activity: Activity = this


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)

        var play_btn : Button = findViewById(R.id.play_btn) as Button
        var play_btn2 : Button = findViewById(R.id.play_btn2) as Button

        play_btn.setOnClickListener {
            DataManager.readData(1)

            val i = Intent(activity, MainActivity::class.java)
            startActivity(i)
        }
    }

}



package breeze.mobile.rebus.util

import io.reactivex.subjects.BehaviorSubject

class Variable<String>(private val defaultValue: String) {
    var value: String = defaultValue
        set(value) {
            field = value
            observable.onNext(value)
        }
    val observable = BehaviorSubject.createDefault(value)
}
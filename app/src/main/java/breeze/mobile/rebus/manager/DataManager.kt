package breeze.mobile.rebus.manager

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.graphics.Point
import android.graphics.Rect
import android.util.Log
import android.util.Size
import breeze.mobile.rebus.MainActivity
import breeze.mobile.rebus.MyApplication
import breeze.mobile.rebus.R
import breeze.mobile.rebus.models.Square
import breeze.mobile.rebus.models.Definition
import breeze.mobile.rebus.util.Variable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

object DataManager{

    var definitions = arrayListOf<Definition?>()
    var id : Int = 0
    var size : Size? = null
    var rightCharacters = arrayOf<Array<String>>()
    var typedCharacters = arrayOf<Array<String>>()

    var currentDirection : Definition.Direction = Definition.Direction.Horizontal
    var currentInput : Square? = null

    var inputs = arrayOf<Array<Square?>>()

    var defText = Variable("")
    fun getObservable(): Observable<String> {
        return defText.observable
    }

    fun readData(rebus: Int){

        val mainJson = JSONObject(loadJSONFromAssets(rebus))
        id = mainJson.getInt("id")
        size = Size(mainJson.getJSONObject("size").getInt("w"), mainJson.getJSONObject("size").getInt("h"))

        definitions = Definition.listItems(mainJson)

        initRightCharacters()

    }

    fun loadJSONFromAssets(rebus: Int): String? {
        var json: String? = null
        try {

            var rebusLink = ""
            if(rebus == 1){
                rebusLink = "rebus1.json"
            }
            val inputStream = MyApplication.applicationContext().assets.open(rebusLink)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()

            json = String(buffer, Charsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return json
    }

    fun squareForCoordinate(p: Point): Square? {
        return inputs[p.y][p.x]
    }

    fun deselectCurrent(){

        if(currentInput!=null){
            currentInput!!.changeSelectedCell(false)

            val d = currentInput!!.definitionForCoordinateAndDirection(currentInput!!.coordinate, currentDirection)

            if(d!=null){
                loop(d, false)
            }
        }

    }

    fun selectCurrent(){

        if(currentInput!=null){

            val d = currentInput!!.definitionForCoordinateAndDirection(currentInput!!.coordinate, currentDirection)

            if(d!=null){
                loop(d, true)
                defText.value = d.definition

            }else
            {
                rotate()
                val d2 = currentInput!!.definitionForCoordinateAndDirection(currentInput!!.coordinate, currentDirection)
                if(d2!=null){
                    loop(d2, true)
                    defText.value = d2.definition
                }
            }
        }

    }

    fun loop(d : Definition, selectedFlag : Boolean){

        when(currentDirection){
            Definition.Direction.Horizontal ->
                for(index in d.answer.indices){
                    val x = index + d.startCoordinate.x
                    val y = d.startCoordinate.y

                    squareForCoordinate(Point(x,y))?.select(selectedFlag)
                }
            Definition.Direction.Vertical ->
                for(index in d.answer.indices){
                    val x = d.startCoordinate.x
                    val y = d.startCoordinate.y + index

                    squareForCoordinate(Point(x,y))?.select(selectedFlag)
                }
        }

    }


    fun coordinateWithData(coordJson: JSONObject): Point?{

        try {

            val x = coordJson.getInt("x")
            val y = coordJson.getInt("y")

            return Point(x, y)

        }catch (e : Exception) {
            e.printStackTrace()
            return null
        }

    }

    fun definitionsForCoordinate(p : Point) : ArrayList<Definition>?{

        val defs = ArrayList<Definition>()

        for(d in definitions) {
            if(d!!.direction == Definition.Direction.Horizontal) {
              val rect = Rect(d.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x + d.answer.length, d.startCoordinate.y+1)

                if(rect.contains(p.x, p.y)){
                    defs.add(d)
                }
            }else
                if(d.direction == Definition.Direction.Vertical){
                val rect = Rect(d.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x+1, d.startCoordinate.y + d.answer.length)

                if(rect.contains(p.x, p.y)){
                    defs.add(d)
                }
            }

        }

        if(defs.size>0){
            return defs
        }else {
            return null
        }
    }

    fun definitionForCoordinateWithDirection(p : Point, dir : Definition.Direction) : Definition?{

        for(d in definitions) {
            if(dir == Definition.Direction.Horizontal) {
                val rect = Rect(d!!.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x + d.answer.length, d.startCoordinate.y+1)

                if(rect.contains(p.x, p.y)){
                    return d
                }
            }else
                if(dir == Definition.Direction.Vertical){
                    val rect = Rect(d!!.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x+1, d.startCoordinate.y + d.answer.length)

                    if(rect.contains(p.x, p.y)){
                        return d
                    }
                }

        }

        return null
    }

    fun definitionForCoordinateBothDirections(p : Point) : Definition?{

        for(d in definitions) {
            if(d?.direction == Definition.Direction.Horizontal) {
                val rect = Rect(d.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x + d.answer.length, d.startCoordinate.y+1)

                if(rect.contains(p.x, p.y)){
                    return d
                }
            }else
                if(d?.direction == Definition.Direction.Vertical){
                    val rect = Rect(d.startCoordinate.x, d.startCoordinate.y, d.startCoordinate.x+1, d.startCoordinate.y + d.answer.length)

                    if(rect.contains(p.x, p.y)){
                        return d
                    }
                }

        }

        return null
    }

    fun initRightCharacters(){
        val square : Square? = null
        for (i in 0 until size!!.height) {
            var array = arrayOf<String>()
            var array2 = arrayOf<String>()
            var arrayInputs = arrayOf<Square?>()
            for (j in 0 until size!!.width) {
                array += ""
                array2 += ""
                arrayInputs += square
            }

            rightCharacters += array
            typedCharacters += array2
            inputs += arrayInputs
        }
    }

    fun rotate(){
        currentDirection = if(currentDirection == Definition.Direction.Horizontal) Definition.Direction.Vertical else Definition.Direction.Horizontal
    }

    fun resetData(){
        definitions.clear()
        for (i in 0 until rightCharacters.size) {
            Arrays.fill(rightCharacters[i], null)
            Arrays.fill(typedCharacters[i], null)
            Arrays.fill(inputs[i], null)
        }
        id = 0
        size = null
        currentInput = null
        currentDirection = Definition.Direction.Horizontal
        defText = Variable("")
    }

}
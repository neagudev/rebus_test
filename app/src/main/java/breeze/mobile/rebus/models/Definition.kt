package breeze.mobile.rebus.models

import android.graphics.Point
import android.util.Log
import breeze.mobile.rebus.manager.DataManager
import org.json.JSONArray
import org.json.JSONObject

class Definition(var startCoordinate: Point, var answer: String, var definition: String, val direction: Direction) {

    enum class Direction {
        Horizontal, Vertical
    }

    companion object {
        fun listItems(mainJson: JSONObject): ArrayList<Definition?> {
            val arrayList = ArrayList<Definition?>()

            val definitionsH: JSONArray = mainJson.getJSONArray("definitionsH")
            val definitionsV: JSONArray = mainJson.getJSONArray("definitionsV")

//            Log.e("SIZE", definitionsH.length().toString() + " " + definitionsV.length().toString())

            for (i in 0 until definitionsH.length()) {
                val def = generateDefinition(definitionsH.getJSONObject(i), Direction.Horizontal)

                if(def!=null){
                    arrayList.add(def)
                }
            }

            for (i in 0 until definitionsV.length()) {
                val def = generateDefinition(definitionsV.getJSONObject(i), Direction.Vertical)

                if(def!=null){
                    arrayList.add(def)
                }
            }


            return arrayList
        }

        fun generateDefinition(defJson: JSONObject, direction: Direction): Definition? {

            val definition: String = defJson.optString("definition")
            val answer: String = defJson.optString("answer")
            val startCoordinate = DataManager.coordinateWithData(defJson.getJSONObject("startCoordinate"))

            return if (definition.isNotEmpty() && answer.isNotEmpty() && startCoordinate != null){
                Definition(startCoordinate, answer, definition, direction)
            }else {
                null
            }
        }
    }



}
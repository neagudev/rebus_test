package breeze.mobile.rebus.models

import android.content.Context
import android.graphics.Point
import androidx.appcompat.widget.AppCompatEditText
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.inputmethod.EditorInfo
import breeze.mobile.rebus.R
import breeze.mobile.rebus.manager.DataManager
import breeze.mobile.rebus.manager.DataManager.currentDirection
import breeze.mobile.rebus.manager.DataManager.currentInput
import breeze.mobile.rebus.manager.DataManager.deselectCurrent
import breeze.mobile.rebus.manager.DataManager.inputs
import breeze.mobile.rebus.manager.DataManager.typedCharacters



class Square : AppCompatEditText {

    lateinit var coordinate : Point
    var definitions : ArrayList<Definition>? = null
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        super.onTouchEvent(event)

        when (event?.action) {
            MotionEvent.ACTION_DOWN ->{
                performClick()
                if(currentInput != null && currentInput==this){
                    deselectCurrent()
                    DataManager.rotate()
                    DataManager.selectCurrent()
                }else
                {
                    if(currentInput !=null){
                        deselectCurrent()
                    }

                    currentInput = this
                    DataManager.selectCurrent()
                }

                changeSelectedCell(true)


                return true
            }

        }
        return false
    }



    override fun performClick(): Boolean {
        super.performClick()

        return true
    }


    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)

        if(hasFocus()) {

            val str = text.toString()
            if (str.length > 1) {
                val newc = text?.get(start + lengthBefore)

                this.setText(newc.toString())

            }

            if (this.text.toString().length == 1) {

                val def = definitionForCoordinateAndDirection(coordinate, currentDirection)

                if (def != null) {
                    if (currentDirection == Definition.Direction.Horizontal) {


                        if (coordinate.x == def.startCoordinate.x + def.answer.length - 1) {
                            //nothing
                        } else {

                            val newInput = inputs[coordinate.y][coordinate.x + 1]
                            currentInput?.changeSelectedCell(false)
                            currentInput = newInput
                            currentInput?.requestFocus()
                            currentInput?.changeSelectedCell(true)
                        }

                    } else {

                        if (coordinate.y == def.startCoordinate.y + def.answer.length - 1) {
                            //nothing
                        } else {
                            val newInput = inputs[coordinate.y + 1][coordinate.x]
                            currentInput?.changeSelectedCell(false)
                            currentInput = newInput
                            currentInput?.requestFocus()
                            currentInput?.changeSelectedCell(true)
                        }

                    }
                }

            }



        }

        if(::coordinate.isInitialized){
//            Log.e("TYPEDCHAR", "CHAR " +coordinate.y + ", " + coordinate.x + " = " + this.text.toString())
            typedCharacters[coordinate.y][coordinate.x] = this.text.toString()
        }


    }


    override fun onEditorAction(actionCode: Int) {
        super.onEditorAction(actionCode)
        if (actionCode == EditorInfo.IME_ACTION_NEXT ) {
            if (currentDirection == Definition.Direction.Horizontal) {

                inputs[coordinate.y][coordinate.x+1]?.requestFocus()

            } else {
                inputs[coordinate.y+1][coordinate.x]?.requestFocus()
            }
        }
    }

    fun changeSelectedCell(isSelected : Boolean){
        this.background = if(isSelected) resources.getDrawable(R.drawable.selected_cell_bg) else resources.getDrawable(
            R.drawable.highlighted_cell_bg)
    }

    fun select(selected :  Boolean){
        if(selected){
            this.background = resources.getDrawable(R.drawable.highlighted_cell_bg)
        }else
        {
            this.background = resources.getDrawable(R.drawable.basic_cell_bg)
        }
    }

    fun definitionForCoordinateAndDirection(p : Point, dir: Definition.Direction) : Definition? {

        if (definitions != null) {
            return when (definitions!!.size) {
                2 -> if (definitions!![0].direction == dir) {
                    definitions!![0]
                } else {
                    definitions!![1]
                }
                1 -> if (definitions!![0].direction == dir) {
                    definitions!![0]
                } else {
                    null
                }
                else -> null
            }
        }

        return null
    }

}

